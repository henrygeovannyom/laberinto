/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.varios;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author GeovanyMantilla
 */
public class Laberinto{
    
    private short laberinto[][];
    
    public Laberinto(String url){
        ArchivoLeerURL leer = new ArchivoLeerURL(url);
        Object datos[] = leer.leerArchivo();
        this.laberinto = new short[datos.length][];
        for (int i = 0; i < datos.length; i++) {
            String[] fila = datos[i].toString().split(";");
            this.laberinto[i] = new short[fila.length];
            for (int j = 0; j < laberinto[i].length; j++) {
                this.laberinto[i][j] = Short.parseShort(fila[j]);
            }
        }
       /*
            AQUI VA A LEER PERO NO FUNCIONA
       */
      
    }
    
    //ESTE NO EXISTE
    public Laberinto(short laberinto[][]){
        this.laberinto = laberinto;
    }
    
    
    public String getCamino(int filaTeseo, int colTeseo, int filaMinTauro, int colMinTauro, int filaSalida, int colSalida){
        int filas = laberinto.length;
        int columnas = laberinto[0].length;
        Short copiaLaberinto[][] = new Short[filas][columnas];
        initCopiaLaberinto(copiaLaberinto, filas, columnas);
        copiaLaberinto[filaTeseo][colTeseo] = 0;
        copiaLaberinto[filaMinTauro][colMinTauro] = Short.MAX_VALUE;
        getMatrizMinima(filaTeseo, colTeseo, filas, columnas, filaTeseo, colTeseo, filaMinTauro, colMinTauro, filaSalida, colSalida, copiaLaberinto);
        if(copiaLaberinto[filaMinTauro][colMinTauro] == Short.MAX_VALUE){
            return "IMPOSIBLE LLEGAR AL MINOTAURO";
        }
        colorearHasta(filaMinTauro, colMinTauro, filas, columnas, filaTeseo, colTeseo, filaMinTauro, colMinTauro, filaSalida, colSalida, copiaLaberinto[filaMinTauro][colMinTauro], copiaLaberinto);
        laberinto[filaMinTauro][colMinTauro] = Short.MAX_VALUE;
        initCopiaLaberinto(copiaLaberinto, filas, columnas);
        copiaLaberinto[filaMinTauro][colMinTauro] = 0;
        copiaLaberinto[filaSalida][colSalida] = Short.MAX_VALUE;
        getMatrizMinima(filaMinTauro, colMinTauro, filas, columnas, filaTeseo, colTeseo, filaMinTauro, colMinTauro, filaSalida, colSalida, copiaLaberinto);
        if(copiaLaberinto[filaSalida][colSalida] == Short.MAX_VALUE){
            return "IMPOSIBLE LLEGAR A LA SALIDA";
        }
        colorearHasta(filaSalida, colSalida, filas, columnas, filaTeseo, colTeseo, filaMinTauro, colMinTauro, filaSalida, colSalida, copiaLaberinto[filaSalida][colSalida], copiaLaberinto);
        laberinto[filaTeseo][colTeseo] = 9;
        laberinto[filaMinTauro][colMinTauro] = -100;
        laberinto[filaSalida][colSalida] = 1000;
        
       /* Imprimes como quedó la matriz
        System.out.println("klgksj");*/
        for(int i = 0; i < filas; i++){
            for(int j = 0; j < columnas; j++){
                System.out.print(laberinto[i][j] + " ");
            }
            System.out.println("");
        }
        
        
        return null;
    }
    
    private void initCopiaLaberinto(Short copiaLaberinto[][], int filas, int columnas){
        for(int i = 0; i < filas; i++){
            for(int j = 0; j < columnas; j++){
                copiaLaberinto[i][j] = laberinto[i][j];
                if(laberinto[i][j] == 0 || laberinto[i][j] == 1){
                    copiaLaberinto[i][j] = Short.MAX_VALUE; 
                }
            }
        }
    }
    
    private void getMatrizMinima(int i, int j, Integer filas, Integer columnas, Integer filaTeseo, Integer colTeseo, Integer filaMinTauro, Integer colMinTauro, Integer filaSalida, Integer colSalida, Short copiaLaberinto[][]){
        int dx[] = {1, -1, 0, 0};
        int dy[] = {0, 0, 1, -1};
        for(int k = 0; k < 4; k++){
            if( (i + dy[k]) >= 0 && (i + dy[k]) < filas && (j + dx[k]) >= 0 && (j + dx[k]) < columnas && copiaLaberinto[i + dy[k]][j + dx[k]] != -1 && copiaLaberinto[i][j] < copiaLaberinto[i+dy[k]][j+dx[k]]){
                copiaLaberinto[i + dy[k]][j + dx[k]] = (short)(copiaLaberinto[i][j]+1);
                getMatrizMinima(i + dy[k], j + dx[k], filas, columnas, filaTeseo, colTeseo, filaMinTauro, colMinTauro, filaSalida, colSalida, copiaLaberinto);
            }
        }
    }
    
    private void colorearHasta(int i, int j, Integer filas, Integer columnas, Integer filaTeseo, Integer colTeseo, Integer filaMinTauro, Integer colMinTauro, Integer filaSalida, Integer colSalida, int curr, Short copiaLaberinto[][]){
        laberinto[i][j] = 1;
        if(curr == 0) return;
        int dx[] = {1, -1, 0, 0};
        int dy[] = {0, 0, 1, -1};
        for(int k = 0; k < 4; k++){
            if( (i + dy[k]) >= 0 && (i + dy[k]) < filas && (j + dx[k]) >= 0 && (j + dx[k]) < columnas && laberinto[i + dy[k]][j + dx[k]] != -1 && copiaLaberinto[i+dy[k]][j+dx[k]] < copiaLaberinto[i][j]){
                colorearHasta(i + dy[k], j + dx[k], filas, columnas, filaTeseo, colTeseo, filaMinTauro, colMinTauro, filaSalida, colSalida, curr - 1, copiaLaberinto);
                return;
            }
        }
    }
    
    
}
